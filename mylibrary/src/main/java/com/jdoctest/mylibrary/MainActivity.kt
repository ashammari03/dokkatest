package com.jdoctest.mylibrary

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

/**
 * This is a KDoc comment.
 * It provides documentation for the following element.
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}