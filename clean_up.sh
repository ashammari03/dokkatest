#!/bin/bash

if [ -d "docs" ]; then
    git fetch origin

    git checkout main
    git reset --hard origin/main

    rm -rf docs

    # Stage and commit the changes
    git add .
    git commit -m "Update documentation URL"

    # Push changes to the remote main branch
    git push origin main

    # Get the version from build.gradle
    VERSION=$(grep -o "versionName \"[0-9.]*\"" app/build.gradle | sed -E 's/versionName "([0-9.]*)"/\1/')

    # Create a tag with the version as the tag name
    git tag -a "$VERSION" -m "Version $VERSION"

    # Push the tag to the remote repository
    git push origin "$VERSION"
else
    echo "No need for documentation clean up"
fi