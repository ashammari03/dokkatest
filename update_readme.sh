#!/bin/bash

# Fetch remote changes
git fetch origin

git checkout main
git reset --hard origin/main

chmod +x ./gradlew
./gradlew copyDokkaOutput


sed -i '' "s#Documentation URL: .*#Documentation URL: $1#" README.md

# Stage and commit the changes
git add .
git commit -m "Update documentation URL"

# Push changes to the remote main branch
git push origin main